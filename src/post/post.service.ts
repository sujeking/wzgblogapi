import { Injectable } from '@nestjs/common';
import { EntityManager } from "typeorm"
import { Post } from './entities/post.entity';


@Injectable()
export class PostService {
    constructor(
        private entityManager: EntityManager
    ) { }

    async create(body: any) {
        try {
            let info = await this.entityManager.save(body)
            if (info) {
                return { code: 200, msg: "success", data: {} }
            } else {
                return { code: 0, msg: "save filed", data: {} }
            }
        } catch (error) {
            return { code: 0, msg: error, data: {} }
        }
    }

    async findAllByTopicId(id: number) {
        try {
            let models: Post[] = await this.entityManager.find(Post, {
                where: { "topic_id": id }
            })
            return { code: 200, msg: "success", data: models }
        } catch (error) {
            return { code: 0, msg: error, data: {} }
        }
    }

    async findAll() {
        try {
            let models = await this.entityManager.find(Post);
            return { code: 200, msg: "success", data: models }
        } catch (error) {
            return { code: 0, msg: error, data: {} }
        }
    }

    async findOne(id: number) {
        try {
            let model: Post = await this.entityManager.findOne(Post,
                { where: { id } })
            return { code: 200, msg: "success", data: model }
        } catch (error) {
            return { code: 0, msg: error, data: {} }
        }
    }

    async update(id: number) {
        try {
            let model: Post = await this.entityManager.findOne(Post,
                { where: { id } })
            return { code: 200, msg: "success", data: model }
        } catch (error) {
            return { code: 0, msg: error, data: {} }
        }
    }

    async remove(id: number) {
        try {
            let model: Post = await this.entityManager.findOne(Post,
                { where: { id } })
            model.is_del = 1;
            let info = await this.entityManager.save(model)
            if (info) {
                return { code: 200, msg: "del failed", data: {} }
            } else {
                return { code: 0, msg: "success", data: {} }
            }
        } catch (error) {
            return { code: 0, msg: error, data: {} }
        }

    }
}
