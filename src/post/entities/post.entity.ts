import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Post {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    uid: string

    @Column()
    topic_id: number

    @Column()
    title: string

    @Column()
    content: string

    @Column({ nullable: true })
    imgs: string

    @UpdateDateColumn()
    update_at: Date

    @CreateDateColumn()
    create_at: Date

    @Column({ default: 0 })
    is_del: number
}
