import { Injectable } from '@nestjs/common';
import { EntityManager } from 'typeorm';
import { Group } from './entities/group.entity';

@Injectable()
export class GroupService {
    constructor(
        private entityManager: EntityManager
    ) { }

    async create(body: any) {
        try {
            let model = new Group()
            model.name = body["name"];
            let info = await this.entityManager.save(model)
            if (info) {
                return { code: 200, msg: "success", data: {} }
            } else {
                return { code: 0, msg: "add filed", data: {} }
            }
        } catch (error) {
            return { code: 0, msg: error, data: {} }
        }
    }

    async findAll() {
        try {
            let info = await this.entityManager.find(Group)
            return { code: 200, msg: "success", data: info }
        } catch (error) {
            return { code: 0, msg: error, data: {} }
        }
    }

    async findOne(id: number) {
        try {
            let info = await this.entityManager.findOne(Group, {
                where: { id }
            })
            return { code: 200, msg: "success", data: info }
        } catch (error) {
            return { code: 0, msg: error, data: {} }
        }

    }

    async update(id: number) {
        try {
            let model: Group = await this.entityManager.findOne(Group, {
                where: { id }
            })
            let info = await this.entityManager.save(model)
            return { code: 200, msg: "success", data: info }
        } catch (error) {
            return { code: 0, msg: error, data: {} }
        }
    }

    async remove(id: number) {
        try {
            let model: Group = await this.entityManager.findOne(Group, {
                where: { id }
            })
            model.is_del = 1;
            let info = await this.entityManager.save(model)
            return { code: 200, msg: "success", data: info }
        } catch (error) {
            return { code: 0, msg: error, data: {} }
        }
    }
}
