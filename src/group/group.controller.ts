import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { GroupService } from './group.service';

@Controller('group')
export class GroupController {
    constructor(private readonly groupService: GroupService) { }

    @Post()
    create(@Body() body: { name: string }) {
        return this.groupService.create(body);
    }

    @Get()
    findAll() {
        return this.groupService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.groupService.findOne(+id);
    }

    @Patch(':id')
    update(@Param('id') id: string,) {
        return this.groupService.update(+id);
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.groupService.remove(+id);
    }
}
