import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityManager, Repository } from 'typeorm';
import { Wallet } from './entities/wallet.entity';
@Injectable()
export class WalletService {
    constructor(
        private entityManager: EntityManager
    ) { }

    async create(model: Wallet) {
        try {
            await this.entityManager.save(model);
            console.log("创建钱包成功");
        } catch (error) {
            console.log("创建钱包失败");
        }
    }

    async findAll() {
        let list = await this.entityManager.find(Wallet, { select: ['balence', 'update_at'] })
        return list
    }

    async findOne(uid: string) {
        let model = await this.entityManager.findOne(Wallet, {
            where: {
                uid
            }
        })
        return model;
    }

    async update(uid: string, model: Wallet) {
        let wallet = await this.entityManager.findOne(Wallet, {
            where: {
                uid
            }
        })
        wallet = { ...model }
        this.entityManager.save(wallet);
        return { code: 200, msg: "update success" }
    }
}
