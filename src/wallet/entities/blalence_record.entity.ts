/// 消费记录

import { Entity, PrimaryGeneratedColumn, CreateDateColumn, Column } from "typeorm"

@Entity()
export class BalenceReCord {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    content: string

    @CreateDateColumn()
    create_at: Date

}