
import { Entity, UpdateDateColumn, Column, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm"


@Entity()
export class Wallet {
    @PrimaryColumn()
    uid: string
    @Column()
    balence: number
    @UpdateDateColumn()
    update_at: Date
}
