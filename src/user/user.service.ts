import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Wallet } from 'src/wallet/entities/wallet.entity';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { v1 as uuidv1 } from "uuid"
import { EntityManager } from "typeorm"

@Injectable()
export class UserService {
    constructor(private readonly entityManager: EntityManager) { }

    async login(body: { user_name: string, pass_word: string }) {
        try {
            let info = await this.entityManager.findOne(User, {
                where: body
            })
            if (info) {
                let bf = Buffer.from(Date.now() + "token" + uuidv1());
                info.token = bf.toString("base64")
                let res = await this.entityManager.save(info);
                return { code: 200, msg: "ok", data: { token: res.token } }
            } else {
                return { code: 0, msg: "用户名或密码错误", data: {} }
            }
        } catch (error) {
            return { code: 0, msg: error, data: {} }
        }
    }

    async create(body: { user_name: string, pass_word: string, phone: string }) {
        let { user_name, pass_word, phone } = body

        let user: User = new User()
        user.user_name = user_name;
        user.pass_word = pass_word;
        user.phone = phone;
        user.uid = "u" + uuidv1()
        let res = await this.entityManager.save(user);
        if (res.uid) {
            let wallet = new Wallet();
            wallet.uid = res.uid;
            wallet.balence = 0.00;
            this.entityManager.save(wallet);
            return { code: 0, msg: "ok", data: { token: res.token } }
        }
        return { code: 400, msg: "regist user faild", data: {} }
    }

    async findAll() {
        try {
            let list = await this.entityManager.find(User, { select: ["user_name", "uid", "phone"] });
            return { code: 0, msg: "success", data: list }
        } catch (error) {
            return { code: 0, msg: "list user faild", data: {} }
        }
    }

    async findUserWithUserNameAndPassword(user_name: string, pass_word: string) {
        let user = await this.entityManager.findOne(User, {
            where: {
                user_name,
                pass_word,
            }
        })
        if (user != null) {
            return user;
        }
        return null;
    }

    async findUserWithToken(token: string) {
        let user = await this.entityManager.findOne(User, { where: { token } })
        if (user) {
            return user;
        }
        return null;
    }

    async update(body: { uid: string }) {
        let { uid } = body;
        try {
            let user = await this.entityManager.findOne(User, { where: { uid } })
            let res = await this.entityManager.save(user);
            return { code: 0, msg: "success", data: res }
        } catch (error) {
            return { code: 0, msg: "update user faild", data: {} }
        }
    }
}
