import { Column, CreateDateColumn, Entity, PrimaryColumn, UpdateDateColumn } from "typeorm"


@Entity()
export class User {

    @Column({ nullable: true })
    group_id: number

    @Column()
    user_name: string

    @Column({ nullable: true })
    nick_name: string

    @Column()
    pass_word: string

    @PrimaryColumn()
    uid: string

    @Column({ nullable: true })
    avatar: string

    @Column({ nullable: true, default: "https://pica.zhimg.com/80/v2-88fc0533339c7058316c285edb03bc4b_1440w.jpg?source=1940ef5c" })
    token: string

    @Column({ nullable: true })
    house: string

    @Column()
    phone: string

    @CreateDateColumn()
    create_at: Date

    @UpdateDateColumn()
    update_at: Date
}
