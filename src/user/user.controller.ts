import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { UserService } from './user.service';
@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService) { }

    @Post("/login")
    async Login(@Body() body: { user_name: string, pass_word: string }) {
        return await this.userService.login(body);
    }

    @Post("/reg")
    async Reg(@Body() body: { user_name: string, pass_word: string, phone: string }) {
        let user = await this.userService.findUserWithUserNameAndPassword(body["user_name"], body["pass_word"])
        if (user != null) {
            return { code: 400, msg: "用户已存在", data: user }
        } else {
            return this.userService.create(body);
        }
    }

    @Post("/update")
    async Update(@Body() body: { uid: string }) {
        return await this.userService.update(body);
    }

    @Get("/list")
    async findAll() {
        return await this.userService.findAll()
    }
}
