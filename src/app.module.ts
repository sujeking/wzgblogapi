import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { OrderModule } from './order/order.module';
import { UserModule } from './user/user.module';
import { WalletModule } from './wallet/wallet.module';
import { PostModule } from './post/post.module';
import { TopicModule } from './topic/topic.module';
import { GroupModule } from './group/group.module';
import { AuthMiddleware } from './auth.middleware';
import { UserService } from './user/user.service';
import { TopicController } from './topic/topic.controller';
import { PostController } from './post/post.controller';

let DBModule = TypeOrmModule.forRoot({
    type: "sqlite",
    database: "shop.db",
    entities: [__dirname + "/**/*.entity{.ts,.js}"],
    synchronize: true,
})

@Module({
    imports: [DBModule, OrderModule, UserModule, WalletModule, PostModule, TopicModule, GroupModule],
    controllers: [AppController],
    providers: [AppService, UserService],
})
export class AppModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer.apply(AuthMiddleware).forRoutes(
            "/user/update",
            "/user/list",
            TopicController,
            PostController)
    }

}
