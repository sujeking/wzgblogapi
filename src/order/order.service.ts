import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { v1 as uuidv1 } from "uuid"
import { Repository } from 'typeorm';
import { Order } from './entities/order.entity';

@Injectable()
export class OrderService {
    constructor(@InjectRepository(Order)
    private orderModel: Repository<Order>
    ) { }

    create() {
        let order = new Order()
        order.goods_name = "商品名称01";
        order.user_name = "本因";
        order.price = "12.90"
        order.order_sn = "0573-3640000"
        this.orderModel.save(order);
        return { code: 200, msg: "success", data: {} }
    }

    async findAll() {
        let list = await this.orderModel.find()
        return { code: 200, msg: "success", data: list }
    }

    async findOne(id: number) {
        let order = await this.orderModel.findOne({
            where: { id }
        });
        if (order) {
            return order
        }
        throw new HttpException("Order Not Found", HttpStatus.NOT_FOUND)
    }

    async update(id: number) {
        let order = await this.orderModel.findOne({ where: { id } });
        order.price = "99.00"
        this.orderModel.save(order)
        let o2 = this.orderModel.findOne({ where: { id } })
        if (o2)
            return { code: 200, msg: "update success", data: order }
        throw new HttpException("Order Not Found", HttpStatus.NOT_FOUND)
    }

    async remove(id: number) {
        let order = await this.orderModel.findOne({ where: { id } });
        order.is_del = 1;
        this.orderModel.save(order)
        let o2 = this.orderModel.findOne({ where: { id } })
        if (order)
            return { code: 200, msg: "del success", data: {} }
        throw new HttpException("Order Not Found", HttpStatus.NOT_FOUND)

    }
}
