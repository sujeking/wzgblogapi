import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { OrderService } from './order.service';

@Controller('order')
export class OrderController {
    constructor(private readonly orderService: OrderService) { }
    add(uid: string) {
        return this.orderService.create();
    }


    @Get("/add")
    create(eateOrderDto) {
        return this.orderService.create();
    }

    @Get("/list")
    findAll() {
        return this.orderService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.orderService.findOne(+id);
    }

    @Get('/update/:id')
    update(@Param('id') id: string) {
        return this.orderService.update(+id);
    }

    @Get('/del/:id')
    remove(@Param('id') id: string) {
        return this.orderService.remove(+id);
    }


}
