import { Column, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm"

@Entity()
export class Order {
    @PrimaryGeneratedColumn()
    id: number
    @Column()
    goods_name: string
    @Column()
    price: string
    @Column()
    order_sn: string
    @Column()
    user_name: string
    @Column({ default: 0 })
    is_del: number

    @UpdateDateColumn()
    update_at: Date
}
