import { Injectable } from '@nestjs/common';
import { EntityManager } from "typeorm"
import { Topic } from './entities/topic.entity';

interface Szw {
    name: string
}


@Injectable()
export class TopicService {
    constructor(
        private entityManager: EntityManager
    ) { }

    async create(body: any) {
        try {
            let model = new Topic()
            model.title = body["title"];
            let info = await this.entityManager.save(model);
            if (info != null) {
                return { code: 200, msg: "success" }
            } else {
                return { code: 0, msg: "failed", data: {} }
            }
        } catch (error) {
            return { code: 0, msg: error, data: {} }
        }
    }

    async search(data: any) {
        try {
            if (Object.keys(data).length == 0) {
                let info = await this.entityManager.find(Topic, {
                    where: { is_del: 0 }, select: [
                        "title", "id"
                    ]
                })
                return { code: 200, msg: "success", data: info }
            } else {
                let title: string = data["title"]
                let info = await this.entityManager.createQueryBuilder(Topic, 'topic')
                    .where("topic.is_del == 0")
                    .andWhere("topic.title like :title", { title: `%${title}%` })
                    .select(["topic.id", "topic.title as name"])
                    .getRawMany()
                return { code: 200, msg: "success", data: info }
            }
        } catch (error) {
            return { code: 0, msg: error, data: {} }
        }
    }

    async findOne(id: number) {
        try {
            let info = await this.entityManager.findOne(Topic, {
                where: { id }
            })
            return { code: 200, msg: "success", data: info }
        } catch (error) {
            return { code: 0, msg: error, data: {} }
        }
    }

    async update(id: number) {
        try {
            let model: Topic = await this.entityManager.findOne(Topic, {
                where: { id }
            })
            return { code: 200, msg: "success", data: model }
        } catch (error) {
            return { code: 0, msg: error, data: {} }
        }
    }

    async remove(id: number) {
        try {
            let model: Topic = await this.entityManager.findOne(Topic, {
                where: { id }
            })
            model.is_del = 1
            return { code: 200, msg: "success", data: model }
        } catch (error) {
            return { code: 0, msg: error, data: {} }
        }
    }
}
