import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Topic {

    @PrimaryGeneratedColumn()
    id: number
    @Column({ default: "0" })
    uid: string
    @Column()
    title: string
    @UpdateDateColumn()
    update_at: Date
    @CreateDateColumn()
    create_at: Date

    @Column({ default: 0 })
    is_del: number
}
