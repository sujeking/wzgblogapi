import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { TopicService } from './topic.service';

@Controller('topic')
export class TopicController {
    constructor(private readonly topicService: TopicService) { }

    @Post()
    create(@Body() body: { title: string }) {
        return this.topicService.create(body);
    }

    @Get()
    search(@Query() data) {
        console.log(data);

        return this.topicService.search(data);
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.topicService.findOne(+id);
    }

    @Patch(':id')
    update(@Param('id') id: string,) {
        return this.topicService.update(+id);
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.topicService.remove(+id);
    }
}
