import { Injectable, NestMiddleware } from '@nestjs/common';
import { UserService } from './user/user.service';
import { Response } from 'express';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
    constructor(
        private userService: UserService
    ) { }
    async use(req: any, res: Response, next: () => void) {
        let { token } = req.headers;
        if (token) {
            let info = await this.userService.findUserWithToken(token);
            if (info) {
                next()
            } else {
                res.send("need login");
            }
            console.log("1111111111111");
        } else {
            res.send("no token");
        }
    }
}
