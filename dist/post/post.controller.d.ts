import { PostService } from './post.service';
export declare class PostController {
    private readonly postService;
    constructor(postService: PostService);
    create(body: any): Promise<{
        code: number;
        msg: any;
        data: {};
    }>;
    findAll(): Promise<{
        code: number;
        msg: string;
        data: import("./entities/post.entity").Post[];
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
    findOne(id: string): Promise<{
        code: number;
        msg: string;
        data: import("./entities/post.entity").Post;
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
    update(id: string): Promise<{
        code: number;
        msg: string;
        data: import("./entities/post.entity").Post;
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
    remove(id: string): Promise<{
        code: number;
        msg: any;
        data: {};
    }>;
}
