export declare class Post {
    id: number;
    uid: string;
    topic_id: number;
    title: string;
    content: string;
    imgs: string;
    update_at: Date;
    create_at: Date;
    is_del: number;
}
