"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const post_entity_1 = require("./entities/post.entity");
let PostService = class PostService {
    constructor(entityManager) {
        this.entityManager = entityManager;
    }
    async create(body) {
        try {
            let info = await this.entityManager.save(body);
            if (info) {
                return { code: 200, msg: "success", data: {} };
            }
            else {
                return { code: 0, msg: "save filed", data: {} };
            }
        }
        catch (error) {
            return { code: 0, msg: error, data: {} };
        }
    }
    async findAllByTopicId(id) {
        try {
            let models = await this.entityManager.find(post_entity_1.Post, {
                where: { "topic_id": id }
            });
            return { code: 200, msg: "success", data: models };
        }
        catch (error) {
            return { code: 0, msg: error, data: {} };
        }
    }
    async findAll() {
        try {
            let models = await this.entityManager.find(post_entity_1.Post);
            return { code: 200, msg: "success", data: models };
        }
        catch (error) {
            return { code: 0, msg: error, data: {} };
        }
    }
    async findOne(id) {
        try {
            let model = await this.entityManager.findOne(post_entity_1.Post, { where: { id } });
            return { code: 200, msg: "success", data: model };
        }
        catch (error) {
            return { code: 0, msg: error, data: {} };
        }
    }
    async update(id) {
        try {
            let model = await this.entityManager.findOne(post_entity_1.Post, { where: { id } });
            return { code: 200, msg: "success", data: model };
        }
        catch (error) {
            return { code: 0, msg: error, data: {} };
        }
    }
    async remove(id) {
        try {
            let model = await this.entityManager.findOne(post_entity_1.Post, { where: { id } });
            model.is_del = 1;
            let info = await this.entityManager.save(model);
            if (info) {
                return { code: 200, msg: "del failed", data: {} };
            }
            else {
                return { code: 0, msg: "success", data: {} };
            }
        }
        catch (error) {
            return { code: 0, msg: error, data: {} };
        }
    }
};
PostService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [typeorm_1.EntityManager])
], PostService);
exports.PostService = PostService;
//# sourceMappingURL=post.service.js.map