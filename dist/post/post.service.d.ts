import { EntityManager } from "typeorm";
import { Post } from './entities/post.entity';
export declare class PostService {
    private entityManager;
    constructor(entityManager: EntityManager);
    create(body: any): Promise<{
        code: number;
        msg: any;
        data: {};
    }>;
    findAllByTopicId(id: number): Promise<{
        code: number;
        msg: string;
        data: Post[];
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
    findAll(): Promise<{
        code: number;
        msg: string;
        data: Post[];
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
    findOne(id: number): Promise<{
        code: number;
        msg: string;
        data: Post;
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
    update(id: number): Promise<{
        code: number;
        msg: string;
        data: Post;
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
    remove(id: number): Promise<{
        code: number;
        msg: any;
        data: {};
    }>;
}
