import { TopicService } from './topic.service';
export declare class TopicController {
    private readonly topicService;
    constructor(topicService: TopicService);
    create(body: {
        title: string;
    }): Promise<{
        code: number;
        msg: string;
        data?: undefined;
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
    search(data: any): Promise<{
        code: number;
        msg: string;
        data: any[];
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
    findOne(id: string): Promise<{
        code: number;
        msg: string;
        data: import("./entities/topic.entity").Topic;
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
    update(id: string): Promise<{
        code: number;
        msg: string;
        data: import("./entities/topic.entity").Topic;
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
    remove(id: string): Promise<{
        code: number;
        msg: string;
        data: import("./entities/topic.entity").Topic;
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
}
