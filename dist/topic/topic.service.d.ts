import { EntityManager } from "typeorm";
import { Topic } from './entities/topic.entity';
export declare class TopicService {
    private entityManager;
    constructor(entityManager: EntityManager);
    create(body: any): Promise<{
        code: number;
        msg: string;
        data?: undefined;
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
    search(data: any): Promise<{
        code: number;
        msg: string;
        data: any[];
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
    findOne(id: number): Promise<{
        code: number;
        msg: string;
        data: Topic;
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
    update(id: number): Promise<{
        code: number;
        msg: string;
        data: Topic;
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
    remove(id: number): Promise<{
        code: number;
        msg: string;
        data: Topic;
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
}
