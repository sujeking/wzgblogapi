"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TopicService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const topic_entity_1 = require("./entities/topic.entity");
let TopicService = class TopicService {
    constructor(entityManager) {
        this.entityManager = entityManager;
    }
    async create(body) {
        try {
            let model = new topic_entity_1.Topic();
            model.title = body["title"];
            let info = await this.entityManager.save(model);
            if (info != null) {
                return { code: 200, msg: "success" };
            }
            else {
                return { code: 0, msg: "failed", data: {} };
            }
        }
        catch (error) {
            return { code: 0, msg: error, data: {} };
        }
    }
    async search(data) {
        try {
            if (Object.keys(data).length == 0) {
                let info = await this.entityManager.find(topic_entity_1.Topic, {
                    where: { is_del: 0 }, select: [
                        "title", "id"
                    ]
                });
                return { code: 200, msg: "success", data: info };
            }
            else {
                let title = data["title"];
                let info = await this.entityManager.createQueryBuilder(topic_entity_1.Topic, 'topic')
                    .where("topic.is_del == 0")
                    .andWhere("topic.title like :title", { title: `%${title}%` })
                    .select(["topic.id", "topic.title as name"])
                    .getRawMany();
                return { code: 200, msg: "success", data: info };
            }
        }
        catch (error) {
            return { code: 0, msg: error, data: {} };
        }
    }
    async findOne(id) {
        try {
            let info = await this.entityManager.findOne(topic_entity_1.Topic, {
                where: { id }
            });
            return { code: 200, msg: "success", data: info };
        }
        catch (error) {
            return { code: 0, msg: error, data: {} };
        }
    }
    async update(id) {
        try {
            let model = await this.entityManager.findOne(topic_entity_1.Topic, {
                where: { id }
            });
            return { code: 200, msg: "success", data: model };
        }
        catch (error) {
            return { code: 0, msg: error, data: {} };
        }
    }
    async remove(id) {
        try {
            let model = await this.entityManager.findOne(topic_entity_1.Topic, {
                where: { id }
            });
            model.is_del = 1;
            return { code: 200, msg: "success", data: model };
        }
        catch (error) {
            return { code: 0, msg: error, data: {} };
        }
    }
};
TopicService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [typeorm_1.EntityManager])
], TopicService);
exports.TopicService = TopicService;
//# sourceMappingURL=topic.service.js.map