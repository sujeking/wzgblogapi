export declare class Topic {
    id: number;
    uid: string;
    title: string;
    update_at: Date;
    create_at: Date;
    is_del: number;
}
