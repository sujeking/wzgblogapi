export declare class Group {
    id: number;
    name: string;
    create_at: Date;
    update_at: Date;
    is_del: number;
}
