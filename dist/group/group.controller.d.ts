import { GroupService } from './group.service';
export declare class GroupController {
    private readonly groupService;
    constructor(groupService: GroupService);
    create(body: {
        name: string;
    }): Promise<{
        code: number;
        msg: any;
        data: {};
    }>;
    findAll(): Promise<{
        code: number;
        msg: string;
        data: import("./entities/group.entity").Group[];
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
    findOne(id: string): Promise<{
        code: number;
        msg: string;
        data: import("./entities/group.entity").Group;
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
    update(id: string): Promise<{
        code: number;
        msg: string;
        data: import("./entities/group.entity").Group;
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
    remove(id: string): Promise<{
        code: number;
        msg: string;
        data: import("./entities/group.entity").Group;
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
}
