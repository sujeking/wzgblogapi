"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const group_entity_1 = require("./entities/group.entity");
let GroupService = class GroupService {
    constructor(entityManager) {
        this.entityManager = entityManager;
    }
    async create(body) {
        try {
            let model = new group_entity_1.Group();
            model.name = body["name"];
            let info = await this.entityManager.save(model);
            if (info) {
                return { code: 200, msg: "success", data: {} };
            }
            else {
                return { code: 0, msg: "add filed", data: {} };
            }
        }
        catch (error) {
            return { code: 0, msg: error, data: {} };
        }
    }
    async findAll() {
        try {
            let info = await this.entityManager.find(group_entity_1.Group);
            return { code: 200, msg: "success", data: info };
        }
        catch (error) {
            return { code: 0, msg: error, data: {} };
        }
    }
    async findOne(id) {
        try {
            let info = await this.entityManager.findOne(group_entity_1.Group, {
                where: { id }
            });
            return { code: 200, msg: "success", data: info };
        }
        catch (error) {
            return { code: 0, msg: error, data: {} };
        }
    }
    async update(id) {
        try {
            let model = await this.entityManager.findOne(group_entity_1.Group, {
                where: { id }
            });
            let info = await this.entityManager.save(model);
            return { code: 200, msg: "success", data: info };
        }
        catch (error) {
            return { code: 0, msg: error, data: {} };
        }
    }
    async remove(id) {
        try {
            let model = await this.entityManager.findOne(group_entity_1.Group, {
                where: { id }
            });
            model.is_del = 1;
            let info = await this.entityManager.save(model);
            return { code: 200, msg: "success", data: info };
        }
        catch (error) {
            return { code: 0, msg: error, data: {} };
        }
    }
};
GroupService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [typeorm_1.EntityManager])
], GroupService);
exports.GroupService = GroupService;
//# sourceMappingURL=group.service.js.map