import { EntityManager } from 'typeorm';
import { Group } from './entities/group.entity';
export declare class GroupService {
    private entityManager;
    constructor(entityManager: EntityManager);
    create(body: any): Promise<{
        code: number;
        msg: any;
        data: {};
    }>;
    findAll(): Promise<{
        code: number;
        msg: string;
        data: Group[];
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
    findOne(id: number): Promise<{
        code: number;
        msg: string;
        data: Group;
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
    update(id: number): Promise<{
        code: number;
        msg: string;
        data: Group;
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
    remove(id: number): Promise<{
        code: number;
        msg: string;
        data: Group;
    } | {
        code: number;
        msg: any;
        data: {};
    }>;
}
