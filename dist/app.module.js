"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const order_module_1 = require("./order/order.module");
const user_module_1 = require("./user/user.module");
const wallet_module_1 = require("./wallet/wallet.module");
const post_module_1 = require("./post/post.module");
const topic_module_1 = require("./topic/topic.module");
const group_module_1 = require("./group/group.module");
const auth_middleware_1 = require("./auth.middleware");
const user_service_1 = require("./user/user.service");
const topic_controller_1 = require("./topic/topic.controller");
const post_controller_1 = require("./post/post.controller");
let DBModule = typeorm_1.TypeOrmModule.forRoot({
    type: "sqlite",
    database: "shop.db",
    entities: [__dirname + "/**/*.entity{.ts,.js}"],
    synchronize: true,
});
let AppModule = class AppModule {
    configure(consumer) {
        consumer.apply(auth_middleware_1.AuthMiddleware).forRoutes("/user/update", "/user/list", topic_controller_1.TopicController, post_controller_1.PostController);
    }
};
AppModule = __decorate([
    (0, common_1.Module)({
        imports: [DBModule, order_module_1.OrderModule, user_module_1.UserModule, wallet_module_1.WalletModule, post_module_1.PostModule, topic_module_1.TopicModule, group_module_1.GroupModule],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService, user_service_1.UserService],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map