import { NestMiddleware } from '@nestjs/common';
import { UserService } from './user/user.service';
import { Response } from 'express';
export declare class AuthMiddleware implements NestMiddleware {
    private userService;
    constructor(userService: UserService);
    use(req: any, res: Response, next: () => void): Promise<void>;
}
