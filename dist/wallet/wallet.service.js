"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WalletService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const wallet_entity_1 = require("./entities/wallet.entity");
let WalletService = class WalletService {
    constructor(entityManager) {
        this.entityManager = entityManager;
    }
    async create(model) {
        try {
            await this.entityManager.save(model);
            console.log("创建钱包成功");
        }
        catch (error) {
            console.log("创建钱包失败");
        }
    }
    async findAll() {
        let list = await this.entityManager.find(wallet_entity_1.Wallet, { select: ['balence', 'update_at'] });
        return list;
    }
    async findOne(uid) {
        let model = await this.entityManager.findOne(wallet_entity_1.Wallet, {
            where: {
                uid
            }
        });
        return model;
    }
    async update(uid, model) {
        let wallet = await this.entityManager.findOne(wallet_entity_1.Wallet, {
            where: {
                uid
            }
        });
        wallet = Object.assign({}, model);
        this.entityManager.save(wallet);
        return { code: 200, msg: "update success" };
    }
};
WalletService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [typeorm_1.EntityManager])
], WalletService);
exports.WalletService = WalletService;
//# sourceMappingURL=wallet.service.js.map