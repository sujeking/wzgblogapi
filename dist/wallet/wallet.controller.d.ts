import { WalletService } from './wallet.service';
export declare class WalletController {
    private readonly walletService;
    constructor(walletService: WalletService);
    create(): string;
    findAll(): Promise<import("./entities/wallet.entity").Wallet[]>;
    findOne(id: string): string;
    update(id: string): string;
    remove(id: string): string;
}
