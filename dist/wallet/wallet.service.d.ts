import { EntityManager } from 'typeorm';
import { Wallet } from './entities/wallet.entity';
export declare class WalletService {
    private entityManager;
    constructor(entityManager: EntityManager);
    create(model: Wallet): Promise<void>;
    findAll(): Promise<Wallet[]>;
    findOne(uid: string): Promise<Wallet>;
    update(uid: string, model: Wallet): Promise<{
        code: number;
        msg: string;
    }>;
}
