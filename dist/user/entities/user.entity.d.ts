export declare class User {
    group_id: number;
    user_name: string;
    nick_name: string;
    pass_word: string;
    uid: string;
    avatar: string;
    token: string;
    house: string;
    phone: string;
    create_at: Date;
    update_at: Date;
}
