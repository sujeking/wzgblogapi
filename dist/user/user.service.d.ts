import { User } from './entities/user.entity';
import { EntityManager } from "typeorm";
export declare class UserService {
    private readonly entityManager;
    constructor(entityManager: EntityManager);
    login(body: {
        user_name: string;
        pass_word: string;
    }): Promise<{
        code: number;
        msg: string;
        data: {
            token: string;
        };
    } | {
        code: number;
        msg: any;
        data: {
            token?: undefined;
        };
    }>;
    create(body: {
        user_name: string;
        pass_word: string;
        phone: string;
    }): Promise<{
        code: number;
        msg: string;
        data: {
            token: string;
        };
    } | {
        code: number;
        msg: string;
        data: {
            token?: undefined;
        };
    }>;
    findAll(): Promise<{
        code: number;
        msg: string;
        data: User[];
    } | {
        code: number;
        msg: string;
        data: {};
    }>;
    findUserWithUserNameAndPassword(user_name: string, pass_word: string): Promise<User>;
    findUserWithToken(token: string): Promise<User>;
    update(body: {
        uid: string;
    }): Promise<{
        code: number;
        msg: string;
        data: User;
    } | {
        code: number;
        msg: string;
        data: {};
    }>;
}
