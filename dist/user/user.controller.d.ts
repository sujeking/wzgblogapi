import { UserService } from './user.service';
export declare class UserController {
    private readonly userService;
    constructor(userService: UserService);
    Login(body: {
        user_name: string;
        pass_word: string;
    }): Promise<{
        code: number;
        msg: string;
        data: {
            token: string;
        };
    } | {
        code: number;
        msg: any;
        data: {
            token?: undefined;
        };
    }>;
    Reg(body: {
        user_name: string;
        pass_word: string;
        phone: string;
    }): Promise<{
        code: number;
        msg: string;
        data: {
            token: string;
        };
    } | {
        code: number;
        msg: string;
        data: {
            token?: undefined;
        };
    }>;
    Update(body: {
        uid: string;
    }): Promise<{
        code: number;
        msg: string;
        data: import("./entities/user.entity").User;
    } | {
        code: number;
        msg: string;
        data: {};
    }>;
    findAll(): Promise<{
        code: number;
        msg: string;
        data: import("./entities/user.entity").User[];
    } | {
        code: number;
        msg: string;
        data: {};
    }>;
}
