"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const wallet_entity_1 = require("../wallet/entities/wallet.entity");
const user_entity_1 = require("./entities/user.entity");
const uuid_1 = require("uuid");
const typeorm_1 = require("typeorm");
let UserService = class UserService {
    constructor(entityManager) {
        this.entityManager = entityManager;
    }
    async login(body) {
        try {
            let info = await this.entityManager.findOne(user_entity_1.User, {
                where: body
            });
            if (info) {
                let bf = Buffer.from(Date.now() + "token" + (0, uuid_1.v1)());
                info.token = bf.toString("base64");
                let res = await this.entityManager.save(info);
                return { code: 200, msg: "ok", data: { token: res.token } };
            }
            else {
                return { code: 0, msg: "用户名或密码错误", data: {} };
            }
        }
        catch (error) {
            return { code: 0, msg: error, data: {} };
        }
    }
    async create(body) {
        let { user_name, pass_word, phone } = body;
        let user = new user_entity_1.User();
        user.user_name = user_name;
        user.pass_word = pass_word;
        user.phone = phone;
        user.uid = "u" + (0, uuid_1.v1)();
        let res = await this.entityManager.save(user);
        if (res.uid) {
            let wallet = new wallet_entity_1.Wallet();
            wallet.uid = res.uid;
            wallet.balence = 0.00;
            this.entityManager.save(wallet);
            return { code: 0, msg: "ok", data: { token: res.token } };
        }
        return { code: 400, msg: "regist user faild", data: {} };
    }
    async findAll() {
        try {
            let list = await this.entityManager.find(user_entity_1.User, { select: ["user_name", "uid", "phone"] });
            return { code: 0, msg: "success", data: list };
        }
        catch (error) {
            return { code: 0, msg: "list user faild", data: {} };
        }
    }
    async findUserWithUserNameAndPassword(user_name, pass_word) {
        let user = await this.entityManager.findOne(user_entity_1.User, {
            where: {
                user_name,
                pass_word,
            }
        });
        if (user != null) {
            return user;
        }
        return null;
    }
    async findUserWithToken(token) {
        let user = await this.entityManager.findOne(user_entity_1.User, { where: { token } });
        if (user) {
            return user;
        }
        return null;
    }
    async update(body) {
        let { uid } = body;
        try {
            let user = await this.entityManager.findOne(user_entity_1.User, { where: { uid } });
            let res = await this.entityManager.save(user);
            return { code: 0, msg: "success", data: res };
        }
        catch (error) {
            return { code: 0, msg: "update user faild", data: {} };
        }
    }
};
UserService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [typeorm_1.EntityManager])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map