import { OrderService } from './order.service';
export declare class OrderController {
    private readonly orderService;
    constructor(orderService: OrderService);
    add(uid: string): {
        code: number;
        msg: string;
        data: {};
    };
    create(eateOrderDto: any): {
        code: number;
        msg: string;
        data: {};
    };
    findAll(): Promise<{
        code: number;
        msg: string;
        data: import("./entities/order.entity").Order[];
    }>;
    findOne(id: string): Promise<import("./entities/order.entity").Order>;
    update(id: string): Promise<{
        code: number;
        msg: string;
        data: import("./entities/order.entity").Order;
    }>;
    remove(id: string): Promise<{
        code: number;
        msg: string;
        data: {};
    }>;
}
