export declare class Order {
    id: number;
    goods_name: string;
    price: string;
    order_sn: string;
    user_name: string;
    is_del: number;
    update_at: Date;
}
