import { Repository } from 'typeorm';
import { Order } from './entities/order.entity';
export declare class OrderService {
    private orderModel;
    constructor(orderModel: Repository<Order>);
    create(): {
        code: number;
        msg: string;
        data: {};
    };
    findAll(): Promise<{
        code: number;
        msg: string;
        data: Order[];
    }>;
    findOne(id: number): Promise<Order>;
    update(id: number): Promise<{
        code: number;
        msg: string;
        data: Order;
    }>;
    remove(id: number): Promise<{
        code: number;
        msg: string;
        data: {};
    }>;
}
